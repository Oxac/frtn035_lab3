function [b,a,the_error] = bafit(z,g,nb,na,weighting)
% BAFIT Fits a rational function to a given process frequency response.
%
%       [B,A,THE_ERROR] = BAFIT(Z,G,NB,NA,WEIGHTING)
%
%       Fits a rational transfer function B(s)/A(s) to the
%       complex frequency response G at the complex frequencies Z
%       using least squares. Deg A = NA and deg B = NB.
%       Weighting may be included (default is unity weighting).
%       The magnitude of the (weighted) closed loop transfer
%       function error is given by THE_ERROR.

% Mats Lilja
% LastEditDate : Thu Feb 28 13:28:20 1991
% Copyright (c) 1990 by Mats Lilja and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

nz = length(z);
ng = length(g);
if nz ~= ng,
  error('Different lengths of vectors Z and G');
end;
if nargin<5,
  weighting = ones(1,nz);
end;
nweight = length(weighting);
if nz ~= nweight,
  error('Wrong number of weightings');
end;
i = sqrt(-1);
f = abs(weighting(:))';
if 2*nz < na+nb+1,
  error('Too few frequencies.');
end;
zj = ones(1,nz);
zb = zj;
for j=1:nb,
  zj = z.*zj;
  zb = [zj ; zb];
end;
zj = ones(1,nz);
za = [];
for j=1:na,
  za = [zj ; za];
  zj = z.*zj;
end;
zn = zj;
ga = diag(g.*f);
gb = diag(f);
m = [-za*ga ; zb*gb];
l = zn*ga;
mm = [imag(m) real(m)];
ll = [imag(l) real(l)];
x = ll/mm;
a = [1 x(1:na)];
b = x(na+1:na+nb+1);
the_error = abs(g - polyval(b,z)./polyval(a,z));
