% FRLSBOX -- A collection of routines for calculating transfer functions
%            and controllers by using least squares fitting in the
%            frequency domain. Some Hankel norm approximation procedures
%            are also included. Written by Mats Lilja.
%
% Least squares fitting
%
%   LSBAC    - Fitting a frequency response to a rational function B(s)/A(s)
%              (continuous time version).
%   LSBAD    - Fitting a frequency response to a rational function B(z)/A(z)
%              (discrete time version).
%   LSBATAU  - Fitting a frequency response to exp(-tau s)B(s)/A(s)
%   LSRSTC   - Calculation of a controller of type Ru = -Sy + Tr
%              by least squares fitting of a closed loop transfer function
%              (continuous time version).
%   LSRSTD   - Calculation of a controller of type Ru = -Sy + Tr
%              by least squares fitting of a closed loop transfer function
%              (discrete time version).
%   BAFIT    - Fitting a frequency response to a rational function.
%              Used by LSBAC and LSBAD.
%   RSTFIT   -  Calculation of a controller of type Ru = -Sy + Tr
%               by least squares fitting of a closed loop transfer function.
%               Used by RSTC and RSTD.
%   
% Frequency response manipulation (these routines are found in FRBOX)
%
%   FPICK     - Pick out points from a frequency response.
%   AMPCROSS  - Compute frequencies of amplitude level crossing.
%   PHACROSS  - Compute frequencies of phase level crossing.
%   BWIDTH    - Compute the bandwidth from a frequency response.
%   ID2FR     - Convert an Ident. Toolbox frequency file to FRBOX format.
%
% Transfer function approximation
%
%   HANKELU  - Unweighted optimal Hankel norm approximation.
%   HANKELW  - Weighted optimal Hankel norm approximation.
%   PADEAPPR - Pade' approximation of exp(-tau s)B(s)/A(s).
%
% Miscellaneous
%
%   SYLVEST   - Compute a Sylvester matrix of two polynomials.
%   LEVCROSS  - Compute level crossings in a table.
%   LEADCOMP  - Compute a lead compensator.
%

% Mats Lilja
% LastEditDate : Thu May 30 09:36:47 1991
% Copyright (c) 1990 by Mats Lilja and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN
