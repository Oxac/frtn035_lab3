function [m] = sylvest(a,b,nx,ny)
% SYLVEST Computes a Sylvester matrix of two polynomials
%
%       M = SYLVEST(A,B,N1,N2)
%
%       The Sylvester matrix of order (N1,N2) is computed
%       for the polynomials A and B.

% Mats Lilja
% LastEditDate : Thu May 30 09:34:24 1991
% Copyright (c) 1990 by Mats Lilja and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

na = length(a);
nb = length(b);
m = na - nb - ny + nx;
a = [zeros(1,-m) a];
b = [zeros(1,m) b];
za = zeros(1,nx-1);
zb = zeros(1,ny-1);
ma = toeplitz([a za],[a(1) za]);
mb = toeplitz([b zb],[b(1) zb]);
m = [ma mb];

