% Mats Lilja
% LastEditDate : Thu Jul 19 19:29:56 1990
% Copyright (c) 1990 by Mats Lilja and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

echo off;
frbox;
ppbox;
clc;
echo on;

%   This session demonstrates methods for designing controllers
%   given some points on the Nyquist curve of a system.
%   In this example the system has the transfer function
%   G(s)=1/(s+1)^8.

b = 1;
a = poly(-ones(1,8));

pause	% Strike RETURN to continue

clc
%   First, an indirect method will be used. The plant transfer
%   function is first approximated by a low order model. A controller
%   based on ordinary pole placement design of the low order plant
%   model is then computed. A third order model should be suitable.
%   Choose approximation frequencies Omega = {0.01 0.2 0.4}:

w = [0.01 0.2 0.4];
f = frc(b,a,0,w);
[bh,ah] = lsbac(f,2,3)

pause	% Strike RETURN to continue

clc
%   A second order controller will be computed by solving the polynomial
%   pole placement (DAB) equation AR + BS = A_m A_o. In order to choose
%   poles of closed loop system (zeros of A_m and A_o), check the poles
%   of the approximate model:

roots(ah)

pause	% Strike RETURN to continue

clc
%   Make the closed loop model poles slightly faster. Take for example

am = polybutt(3,0.5)
ao = polybutt(2,1)
[r,s,t] = rstc(1,bh,ah,1,am,ao,1)

pause	% Strike RETURN to continue

clc
%   Check stability of the actual closed loop:

clpoles = roots(addpoly(conv(a,r),conv(b,s)))

pause   % Strike RETURN to continue

%   Check Nyquist curves of the specified and actual closed loop
%   transfer function (ref --> y):

frcl = frc(conv(b,t),addpoly(conv(a,r),conv(b,s)),0,-2,2,100);
bm = b/b(length(b))*am(length(am));
frm = frc(bm,am,0,-2,2,100);

pause   % Strike RETURN to see closed loop Nyquist curves

nypl(frcl,frm,w);
nygrid; pause

%   Check closed loop performance:

tryu = yusimc(b,a,r,s,t,160);

pause;  % Strike RETURN to view the closed loop time response

yupl(tryu); pause

pause;  % Strike RETURN to continue

clc
%   Introduce integration in the controller. The degree of the
%   polynomial A_m A_o must be increased. Choose to increase deg A_o:

ao = polybutt(3,1)
[r,s,t] = rstc(1,bh,ah,1,am,ao,[1 0])

%   The controller is in this case unstable.

pause;  % Strike RETURN to continue

clc
%   Check closed loop stability:

clpoles = roots(addpoly(conv(a,r),conv(b,s)))

%   The actual closed loop is unstable!

pause;  % Strike RETURN to continue

clc
%   Respecify the performance requirements!

am = polybutt(3,0.4);
ao = polybutt(3,0.7);
[r,s,t] = rstc(1,bh,ah,1,am,ao,[1 0])
pause;

%   Is the system closed loop stable?

clpoles = roots(addpoly(conv(a,r),conv(b,s)))

pause;  % Strike RETURN to continue

clc
%   Check closed loop Nyquist curves.

frcl = frc(conv(b,t),addpoly(conv(a,r),conv(b,s)),0,-2,2,100);
bm = bh/bh(length(bh))*am(length(am)); % Stationary gain G_m(0)=1
frm = frc(bm,am,0,-2,2,100);

pause;  % Strike RETURN to see the Nyquist curves

nypl(frcl,frm,w);
nygrid; pause

%   Closed loop performance?

tryu = yusimc(b,a,r,s,t,160);

pause;  % Strike RETURN to have a look at the closed loop response

yupl(tryu); pause

%   To check robustness the open loop Nyquist curve is plotted.

frl = frc(conv(b,s),conv(a,r),0,-2,2,100);

pause;  % Strike RETURN to see the Nyquist curve of the loop.

nypl(frl,w);
nygrid; pause

pause;  % Strike RETURN to continue

clc
%   Using the indirect design method sketched above, the computation
%   of a second order controller with integration requires a second
%   order process model.

[bh,ah] = lsbac(f,1,2,[1 0.3 0.1])

pause;

%   Specify nominal closed loop poles in butterworth patterns:

am = polybutt(2,0.3);
ao = polybutt(2,0.5);
[r,s,t] = rstc(1,bh,ah,1,am,ao,[1 0])

pause;  % Strike RETURN to continue

clc
%   Check closed loop Nyquist curves.

frcl = frc(conv(b,t),addpoly(conv(a,r),conv(b,s)),0,-2,2,100);
bm = bh/bh(length(bh))*am(length(am));
frm = frc(bm,am,0,-2,2,100);

pause;  % Strike RETURN to see closed loop Nyquist curves

nypl(frcl,frm,w);
nygrid; pause

%   The curve fitting is bad at higher frequencies.

pause;  % Strike RETURN to continue

clc
%   Check the closed loop performance.

tryu = yusimc(b,a,r,s,t,160);

pause;  % Strike RETURN to view the closed loop time response

yupl(tryu); pause

%   The performance is not very impressive.

pause;  % Strike RETURN to continue

clc
%   In the direct method the controller parameters are computed
%   directly from frequency response data without solving
%   any polynomial equation.
%   This makes it possible to use a third order closed loop
%   specification when computing a second order controller
%   with integration. A third order process model is first
%   computed.

[bh,ah] = lsbac(f,2,3)

pause;  % Strike RETURN to continue

clc
%   The numerator polynomial from this transfer function is
%   used in the third order specification.

am = polybutt(3,0.4);
bm = bh/bh(length(bh))*am(length(am));
[r,s,t] = lsrstc(f,bm,am,ao,2,2,[1 0])

pause;  % Strike RETURN to continue

clc
%   Check closed loop Nyquist curves.

frcl = frc(conv(b,t),addpoly(conv(a,r),conv(b,s)),0,-2,2,100);
frm = frc(bm,am,0,-2,2,100);

pause;  % Strike RETURN to see Nyquist curves

nypl(frcl,frm,w);
nygrid; pause

%   The curve fitting is better in this case.

pause;  % Strike RETURN to contimue

%   What about the closed loop performance?

tryu = yusimc(b,a,r,s,t,160);
yupl(tryu); pause

pause;

%   The performance is better than in the indirect method.
echo off

