function [b,a,the_error] = lsbad(fr,h,nb,na,weighting)
% LSBAD Fits a rational function to a given discrete time
%       process frequency response.
%
%       [B,A,THE_ERROR] = LSBAD(FR,H,NB,NA,WEIGHTING)
%
%       Fits a rational transfer function B(z)/A(z) to the
%       discrete time frequency response FR using least squares.
%       The frequency response FR is assumed to be of the form
%       [w G(exp(iwh))] where w is the approximation frequency vector.
%       The sampling interval is specified by H.
%       Deg A = NA and deg B = NB. Weighting may be included (default is
%       unity weighting). The magnitude of the (weighted) closed
%       loop transfer function error is given by THE_ERROR.

%       1991-01-16: Added sampling interval as argument

% Mats Lilja
% LastEditDate : Fri Feb  8 19:10:21 1991
% Copyright (c) 1990 by Mats Lilja and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

nfr = size(fr)*[1;0];
if nargin < 5,
  weighting = ones(1,nfr);
end;
nweight = length(weighting);
if nfr ~= nweight,
  error('Wrong number of weightings');
end;
i = sqrt(-1);
w = fr(:,1).';
z = exp(i*w*h);
g = fr(:,2).';
[b,a,the_error] = bafit(z,g,nb,na,weighting);
