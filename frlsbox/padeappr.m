function [bh,ah] = padeappr(b,a,tau,nbh,nah)
% PADEAPPR Compute a Pade' approximation of (rational function)*exponential
%
%        [BH,AH] = PADEAPPR(B,A,TAU,NBH,NAH)
%
%        Calculates a Pade' approximation BH(s)/AH(s) with
%        deg BH = NBH and deg AH = NAH of the system
%
%           -TAU s  B(s)
%          e       ------
%                   A(s)

% Mats Lilja
% LastEditDate : Thu May 30 09:35:47 1991
% Copyright (c) 1990 by Mats Lilja and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

exppol = 1;
taylor_term = 1;
n = nah + nbh;
na = length(a) - 1;
for j=1:n,
  taylor_term = taylor_term*tau/j;
  exppol = [taylor_term exppol];
end;
e = conv(a,exppol);
e(1:na) = [];
m = sylvest(b,-e,nah+1,nbh+1);
m(1:nbh,:) = [];
l = -m(:,1);
m(:,1) = [];
x = (m\l)';
ah = [1 x(1:nah)];
bh = x(nah+1:nah+nbh+1);

