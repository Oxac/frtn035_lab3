function [r,s,t,the_error]=rstfit(z,g,gm,ao,nr,ns,r1,s1,weighting)
% RSTFIT Fits a controller to a specified closed loop model
%       given a frequency response of the process.
%
%       [R,S,T,THE_ERROR]=RSTFIT(Z,G,GM,AO,NR,NS,R1,S1,WEIGHTING)
%
%       The complex frequency response G, given at the
%       complex frequencies Z, is used for least squares fitting
%       of controller parameters in the controller structure
%       Ru = Tr - Sy. The closed loop system (r --> y) is specified
%       by the frequency response GM, given at the frequencies Z.
%       Factors to be included in R are specified by the polynomial R1.
%       Similarly, S1 pre-specifies factors of S.
%       Deg R = NR and deg S = NS. The observer polynomial is given by
%       AO (T = const.*AO). Default WEIGHTING is unity weighting
%       while R1 and S1 both defaults to 1.
%       The magnitude of the (weighted) closed loop transfer
%       function error is given by THE_ERROR.

% Mats Lilja
% LastEditDate : Mon Jan 28 14:20:00 1991
% Copyright (c) 1990 by Mats Lilja and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

nz = length(z);
ng = length(g);
if nz ~= ng,
  error('Different lengths of vectors Z and G');
end;
if nargin < 9,
  weighting = ones(1,nz);
end;
if nargin < 8,
  s1 = 1;
end;
if nargin < 7,
  r1 = 1;
end;
nweight = length(weighting);
if nz ~= nweight,
  error('Wrong number of weightings');
end;
nr2 = nr - length(r1) + 1;
if nr2 < 0,
  error('Degree of R1 higher than degree of R');
end;
ns2 = ns - length(s1) + 1;
if ns2 < 0,
  error('Degree of S1 higher than degree of S');
end;
i = sqrt(-1);
f = abs(weighting);
Npoints = 2*nz;
Nparameters = nr2 + ns2 + 2;
%%%%
if Npoints < Nparameters
  error('Too few approximation points.');
end;
aovec = polyval(ao,z);
gr = diag(f.*polyval(r1,z).*gm./aovec./g);
gs = diag(f.*polyval(s1,z).*gm./aovec);
one = ones(1,nz);
ms = one;
zs = one;
for j=1:ns2,
  zs = z.*zs;
  ms = [zs ; ms];
end;
mr = one;
zr = one;
for j=1:nr2,
  zr = z.*zr;
  mr = [zr ; mr];
end;
l = f(:).';
m = [mr*gr ; ms*gs];
mm = [real(m) imag(m)];
ll = [real(l) imag(l)];
x = ll/mm;
t0 = 1/x(1);
r2 = [1 t0*x(2:nr2+1)];
r = conv(r1,r2);
s2 = t0*x(nr2+2:nr2+ns2+2);
s = conv(s1,s2);
t = t0*ao;
num = g.*polyval(t,z);
den = polyval(r,z) + g.*polyval(s,z);
the_error = abs(num./den - gm);
