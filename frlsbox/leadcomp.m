function [s,r,wc,phi_m] = leadcomp(fr,n_speed,maxlead,zeta)
% LEADCOMP Compute a lead compensator from a frequency response
%
%       [S,R] = LEADCOMP(FR,N_SPEED)
%
%       Given a frequency response FR, a lead compensator
%       S(s)/R(s) is computed which increases the cut frequency by a
%       factor of N_SPEED without changing the phase margin.
%       The compensator consists of cascaded identical first order
%       compensators, where the number of compensators is determined
%       by the amount of phase lead required.
%
%       [S,R] = LEADCOMP(FR,N_SPEED,MAXLEAD)
%
%       limits the maximum phase lead for each first order compensator
%       to be MAXLEAD (default value = 90 degrees).
%
%       [S,R] = LEADCOMP(FR,N_SPEED,MAXLEAD,ZETA)
%
%       gives a number of identical second order compensators,
%       each with a relative damping ZETA.
%
%       [S,R,OMEGA_C,PHI_M] = LEADCOMP(FR,N_SPEED)
%
%       gives the (old) cut frequency OMEGA_C and the phase margin PHI_M.

% Mats Lilja
% LastEditDate : Thu Mar 15 17:08:46 1990
% Copyright (c) 1990 by Mats Lilja and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

if nargin < 3,
  maxlead = 90;
end;
if nargin < 4,
  zeta = 1;
end;
w = fr(:,1);
g = fr(:,2);
[wc,phi1] = ampcross(fr,1);
phi_m = 180 + phi1;
wc2 = wc*n_speed;
gwc2 = table1([w abs(g) arg(g)],wc2);
amp2 = gwc2(1);
phi2 = gwc2(2);
dphi = phi1 - phi2;
if zeta == 1,
  order = ceil(dphi/maxlead);
  th = pi*(dphi/order+90)/360;
  N = (tan(th))^2;
  b = wc2/sqrt(N);
  r = poly(-N*b*ones(1,order));
  s = poly(-b*ones(1,order))*(sqrt(N))^order/amp2;
else,
  order2 = ceil(dphi/maxlead/2);
  th = pi*(dphi/order2)/360;
  alfa = zeta*tan(th);
  N = (alfa+sqrt(1+alfa^2))^2;
  b = wc2/sqrt(N);
  one2 = ones(1,order2);
  r = polyc(N*b*one2,zeta*one2);
  s = polyc(b*one2,zeta*one2)*(N^order2)/amp2;
end;
