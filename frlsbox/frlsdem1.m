% Mats Lilja
% LastEditDate : Thu Sep 13 14:51:19 1990
% Copyright (c) 1990 by Mats Lilja and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

echo off
frbox
ppbox
clc
echo on

%   In this session, functions in FRLSBOX will be used
%   for approximation of the transfer function G(s)=1/(s+1)^8.

b = 1;
a = poly(-ones(1,8));

%   The first approximation method will be least squares
%   fitting of some points on the Nyquist curve to a rational
%   function of lower order.
%   Generate the frequency response of the system.

fr = frc(b,a,0,-2,2,100);

pause   % Strike RETURN to see the Nyquist plot of G

nypl(fr);
nygrid; pause

pause	% Strike RETURN to continue

clc
%   First, the frequencies for which the Nyquist curve is to be fitted
%   must be chosen. To see this, the axis intersection frequencies
%   of the Nyquist curve are computed.

waxes = table1([arg(fr(:,2)) fr(:,1)],-[90 180 270 360]),

pause	% Strike RETURN to continue

clc
%   Let's start with a second order approximation.
%   with relative degree one. This means that the
%   numerator polynomial is of degree one and the
%   denominator polynomial of degree 2, i.e. a
%   rational approximation of degree (1,2).
%   To see the effect of choosing too high approximation
%   frequencies the (-360) degree frequency 1 rad/s is included.
%   Try, for example, Omega={0.1 0.3 1}.

w = [0.1 0.3 1];
f = frc(b,a,0,w);
[bh,ah] = lsbac(f,1,2)

%   Notice that the approximation is unstable.
%   Calculate the frequency response of the approximation

frh = frc(bh,ah,0,-2,2,100);

pause   % Strike RETURN to see the Nyquist plots of both systems

nypl(fr,frh,w);
nygrid;pause;

%   The curve fit is rather bad.

pause;  % Strike RETURN to continue.

clc
%   Increase model order to 3 (a (2,3) approximation).

[bh,ah] = lsbac(f,2,3)

%   Calculate the frequency response

frh = frc(bh,ah,0,-2,2,100);

pause   % Strike RETURN to see the Nyquist plots

nypl(fr,frh,w);
nygrid; pause

%   Slightly better ...

pause   % Strike RETURN to continue

clc
%   Another way to get a better approximation
%   is to decrease the approximation frequencies.
%   Choose, for example Omega = {0.1 0.2 0.4} and return to
%   approximation by a second order transfer function.
%   Notice that 0.4 is near the (-180) degree frequency.

w = [0.1 0.2 0.4];
f = frc(b,a,0,w);
[bh,ah] = lsbac(f,1,2)
frh = frc(bh,ah,0,-2,2,100);

pause   % Strike RETURN to see the Nyquist plots

nypl(fr,frh,w);
nygrid; pause

pause   % Strike RETURN to continue

%   The error is rather large at the lowest frequency
%   Introducing the frequency weighting {1 0.3 0.1} gives:

[bh,ah] = lsbac(f,1,2,[1 0.3 0.1])
frh = frc(bh,ah,0,-2,2,100);

pause   % Strike RETURN to see the new Nyquist curve

nysh(frh,w,'-.'); pause

pause   % Strike RETURN to continue

clc
%   Increasing approximation model order to three gives:

[bh,ah] = lsbac(f,2,3)
frh = frc(bh,ah,0,-2,2,100);

pause   % Strike RETURN to see the Nyquist plots

nypl(fr,frh,w);
nygrid; pause

%   The Nyquist curves fit very well together.
%   To get a better view of how well, the absolute error is plotted

pause   % Strike RETURN to see the approximation error magnitude

ampl(fsub(fr,frh));
amgrid;  pause

pause   % Strike RETURN to continue

clc
%   The error magnitude has a peak around 1 rad/s
%   An approximation method which gives a much more
%   uniform error magnitude is optimal Hankel norm
%   approximation. A third order (unweighted)
%   approximation is computed and the error magnitude
%   curve is displayed.

[bh,ah] = hankelu(b,a,3)

frh = frc(bh,ah,0,-2,2,100);

pause   % Strike RETURN to see the error magnitude

amsh(fsub(fr,frh),'--'); pause;

%   It is also possible to shape the error by using
%   frequency weighted Hankel norm approximation.
%   Assume that the error should be approximately 4 times
%   smaller at frequencies below 1 rad/s than at frequencies
%   above 1 rad/s. The weighting is then chosen as (s - 2)/(s - 0.5).

[bh,ah] = hankelw(b,a,3,[1 -2],[1 -0.5])
pause;
frh = frc(bh,ah,0,-2,2,100);

pause   % Strike RETURN to see the error magnitude

amsh(fsub(fr,frh),'--'); pause

pause   % Strike RETURN to continue

clc
%   A classical approximation method is Pade' approximation.
%   In this method a rational function Gh(s) of degree (m,n) is
%   calculated such that G(s) - Gh(s) = O(s^(m+n+1)) which
%   means that n+m first taylor coefficient of G(s) and Gh(s)
%   coincide. Let's compute a Pade' approximation of degree (2,3).

[bh,ah] = padeappr(b,a,0,2,3)
frh = frc(bh,ah,0,-2,2,100);

pause   % Strike RETURN to see the error magnitude

ampl(fsub(fr,frh)); pause

%   Notice the small error at low frequecncies.

pause;  % Strike RETURN to continue

clc
%   It is possible to include a time delay in the approximate model.
%   This is done by using a non-linear least squares method.
%   A second order system with time delay is fitted to G(s)
%   at the frequencies Omega = {0.1 0.3 1} with unity weighting (default).

w = [0.1 0.3 1]
f = frc(b,a,0,w);
[bh,ah,tauh] = lsbatau(f,1,2)

%   Unfortunately, this gives a negative time delay.
%   The Nyquist curve fitting becomes very strange.

frh = frc(bh,ah,tauh,-2,2,100);

pause;  % Strike RETURN to see Nyquist plots

nypl(fr,frh,w);
nygrid; pause

pause;  % Strike RETURN to continue

clc
%   One remedy this is to decrease weighting of
%   higher frequencies. Weightings are chosen as {1 0.5 0.1}.

[bh,ah,tauh] = lsbatau(f,1,2,[1 0.5 0.1])

%   A positive time delay is obtained.

frh = frc(bh,ah,tauh,-2,2,100);

pause;  % Strike RETURN to see Nyquist plots

nypl(fr,frh,w);
nygrid; pause

pause;  % Strike RETURN to continue

clc
%   Alternatively, the start value of the time delay (default=0)
%   can be slightly increased. Choose the value 0.1, for example:

[bh,ah,tauh] = lsbatau(f,1,2,[1 1 1],0.1)
frh = frc(bh,ah,tauh,-2,2,100);

pause;  % Strike RETURN to see new Nyquist plot

nysh(frh,w,':'); pause

%   Notice the bad fitting at low frequencies due to the unity
%   weighting {1 1 1} used in this case.

echo off;
