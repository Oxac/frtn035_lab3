function [b,a,tau,the_error,logging] = lsbatau(fr,nb,na,weighting,tau0,dtaumax)
% LSBATAU Least squares fitting of a rational function with
%       a time delay.
%
%       [B,A,TAU,THE_ERROR,LOGGING] = LSBATAU(FR,NB,NA,WEIGHTING,TAU0,DTAUMAX)
%
%       Computes a least-squares fitting
% 
%                    - TAU s  B(s)
%            G(s) = e         ----
%                             A(s)
% 
%       to the frequency response data given in FR.
%       where the degrees of the polynomials A and B are NA and NB
%       respectively. The time delay TAU is found by using a
%       modified Newton-Raphson algorithm. Weighting may be included
%       (default is unity weighting). DTAUMAX is the iteration termination
%       treshold value for the magnitude of the time delay increment
%       (default value = 0.01). TAU0 is the initial value of TAU
%       (default value = 0). The magnitude of the (weighted) closed
%       loop transfer function error is given by THE_ERROR.
%       The history of the iterations are saved in LOGGING.

% Mats Lilja
% LastEditDate : Wed May 22 16:44:16 1991
% Copyright (c) 1990 by Mats Lilja and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

nfr = size(fr)*[1;0];
if nargin < 4,
  weighting = ones(1,nfr);
end;
if nargin < 5,
  tau0 = 0;
end;
if nargin < 6,
  dtaumax = 0.01;
end;
nweight = length(weighting);
if nweight == 0,
  weighting = ones(1,nfr);
elseif nfr ~= nweight,
  error('Wrong number of weightings');
end;
i = sqrt(-1);
w = fr(:,1).';
zvec = i*w;
gvec = fr(:,2).';
pvec = abs(weighting(:))';
if 2*nfr < na+nb+2,
  error('Too few frequencies.');
end;
nn = 2*nfr;
en = eye(nn);
s = conj([zvec conj(zvec)])';
pw = pvec/max(pvec);
filt = diag([pw pw]);
g = diag([gvec conj(gvec)]);
zj = ones(nn,1);
psib = zj;
for j=1:nb;
  zj = s.*zj;
  psib = [zj psib];
end;
zj = ones(nn,1);
psia = zj;
for j=1:(na-1);
  zj = s.*zj;
  psia = [zj psia];
end;
zn = s.*zj;
gam = filt*g*zn;
tau = tau0;
dtau = 10000;
logg = [];
my = 1;
while abs(dtau)>dtaumax,
  count = count + 1;
  ew = diag(exp(-s*tau));
  fi = filt*[-g*psia ew*psib];
  [u,sig,v] = svd(fi);
  u1 = u(:,1:min(size(sig)));
  sig = sig(1:min(size(sig)),:);
  fiff = (v/sig)*u1';
  dew = -diag(s)*ew;
  dfi = filt*[ 0*psia dew*psib];
  d2ew = -diag(s)*dew;
  d2fi = filt*[ 0*psia d2ew*psib];
  p = en - fi*fiff;
  p1 = dfi*fiff;
  p2 = d2fi*fiff;
  q =  -p*p1;
  dp = q + q';
  dq = p*( 2*p1*p1 - p2 ) + q'*q + q*q';
  d2p = dq + dq';
  gpg = gam'*p*gam;
  gdpg = gam'*dp*gam;
  gd2pg = gam'*d2p*gam;
  logg = [logg ; [tau gpg gdpg gd2pg]];
  dtau = -real(gdpg/(0.6*abs(gd2pg)+0.4*gd2pg));
  tau = tau + dtau;
  dtau;
  tau;
end;
ew = diag(exp(-s*tau));
fi = filt*[-g*psia ew*psib];
[u,s,v] = svd(fi);
sig = s(1:min(size(s)),:);
u1 = u(:,1:min(size(s)));
fiff = (v/sig)*u1';
theta = real(fiff*gam);
logging = real(logg);
a = [1 theta(1:na)'];
b = theta(na+1:na+nb+1)';
tau = real(tau);
the_error = abs(gvec - exp(-tau*zvec).*polyval(b,zvec)./polyval(a,zvec));
