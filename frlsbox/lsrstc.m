function [r,s,t,the_error]=lsrstc(fr,bm,am,ao,nr,ns,r1,s1,weighting)
% LSRSTC Fits a continuous time controller to a specified closed
%       loop model given a frequency response of the process.
%
%       [R,S,T,THE_ERROR]=LSRSTC(FR,BM,AM,AO,NR,NS,R1,S1,WEIGHTING)
%
%       The frequency response FR on the form [w G(iw)]
%       is used for least squares fitting of controller parameters
%       in a controller structure given by Ru = Tr - Sy.
%       The closed loop system (r --> y) is specified
%       by the transfer function BM(s)/AM(s). For convenience, the
%       polynomial BM is multiplied by a constant factor in order to
%       get a closed loop stationary gain of 1 (i.e. BM(0)/AM(0)=1).
%       Factors to be included in R are specified by the polynomial R1.
%       Similarly, S1 pre-specifies factors of S.
%       Deg R = NR and deg S = NS. The observer polynomial is given by
%       AO (T = const.*AO). Default WEIGHTING is unity weighting
%       while R1 and S1 both defaults to 1.
%       The magnitude of the (weighted) closed loop transfer
%       function error is given by THE_ERROR.

% Mats Lilja
% LastEditDate : Thu Jul 19 18:20:48 1990
% Copyright (c) 1990 by Mats Lilja and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

nfr = size(fr)*[1;0];
if nargin < 9,
  weighting = ones(1,nfr);
end;
if nargin < 8,
  s1 = 1;
end;
if nargin < 7,
  r1 = 1;
end;
i = sqrt(-1);
w = fr(:,1).';
z = i*w;
g = fr(:,2).';
% To get staionary gain = 1 from r to y
bm = bm/bm(length(bm)).*am(length(am)); 
gm = polyval(bm,z)./polyval(am,z);
[r,s,t,the_error]=rstfit(z,g,gm,ao,nr,ns,r1,s1,weighting);
