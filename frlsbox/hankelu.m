function [Bhat,Ahat,sigm,signature] = hankelu(B,A,nred)
% HANKELU Compute a unweighted Hankel norm approximation
%         of a rational transfer function
%
%         [BHAT,AHAT,SIGM] = HANKELU(B,A,NRED)
%
%         An unweighted optimal Hankel norm approximation BHAT(s)/AHAT(s)
%         of order NRED is computed for B(s)/A(s). The vector SIGM contains
%         the Hankel singular values.

% Mats Lilja
% LastEditDate : Thu May 30 09:35:06 1991
% Copyright (c) 1991 by Mats Lilja and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

na = length(A);
nb = length(B);
m1 = sylvest(A,-B,na-1,na-1);
m2 = diag(1-rem(1:2*na-2,2)*2)*sylvest(0,A,na-1,na-1);
[alfa,beta,q,z,v] = qz(m1,m2);
alfa = diag(alfa);
beta = diag(beta);
nn = na:2*na-2;
d = alfa(nn)./beta(nn);
v = v(:,nn);
[dd,index] = sort(-abs(d));
d = d(index);
v = v(:,index);
sigm = abs(d);
signature = d./sigm;
v = real(v/diag(v(na,:)));
bh = v(1:na-1,nred+1)';
ah = v(na:2*na-2,nred+1)';
[bh,ah] = stabparc(bh,ah);
% This gives the unique strictly proper optimal Hankel norm approximation.
% Next we choose a direct term so that the error magnitude at omega=infinity
% equals the error magnitude at omega=0. This is a heuristic attempt to
% minimize the Tchebycheff norm of the error.
G0 = B(nb)/A(na);
if nb < na,
  Ginf = 0;
else
  Ginf = B(1)/A(1);
end;
Gh0 = bh(length(bh))/ah(length(ah));
Ahat = ah;
direct_term = (Ginf + G0 - Gh0)/2;
Bhat = [0 bh] + ah*direct_term;
