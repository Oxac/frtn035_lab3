function [r,s,t,the_error]=lsrstd(fr,h,bm,am,ao,nr,ns,r1,s1,weighting)
% LSRSTD Fits a discrete time controller to a specified closed
%       loop model given a frequency response of the process.
%
%       [R,S,T,THE_ERROR]=LSRSTD(FR,H,BM,AM,AO,NR,NS,R1,S1,WEIGHTING)
%
%       The frequency response FR on the form [w G(exp(iwh))]
%       is used for least squares fitting of controller parameters
%       in a controller structure given by Ru = Tr - Sy.
%       The sampling interval is specified by H.
%       The closed loop system (r --> y) is specified
%       by the transfer function BM(z)/AM(z). For convenience, the
%       polynomial BM is multiplied by a constant factor in order to
%       get a closed loop stationary gain of 1 (i.e. BM(1)/AM(1)=1).
%       Factors to be included in R are specified by the polynomial R1.
%       Similarly, S1 pre-specifies factors of S.
%       Deg R = NR and deg S = NS. The observer polynomial is given by
%       AO (T = const.*AO). Default WEIGHTING is unity weighting
%       while R1 and S1 both defaults to 1.
%       The magnitude of the (weighted) closed loop transfer
%       function error is given by THE_ERROR.

%       1990-12-03: Added sampling interval as argument

% Mats Lilja
% LastEditDate : Wed Jan 16 14:07:12 1991
% Copyright (c) 1990 by Mats Lilja and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

nfr = size(fr)*[1;0];
if nargin < 10,
  weighting = ones(1,nfr);
end;
if nargin < 9,
  s1 = 1;
end;
if nargin < 8,
  r1 = 1;
end;
i = sqrt(-1);
w = fr(:,1).';
z = exp(i*w*h);
g = fr(:,2).';
% To get staionary gain = 1 from r to y
bm = bm/sum(bm)*sum(am);
gm = polyval(bm,z)./polyval(am,z);
[r,s,t,the_error]=rstfit(z,g,gm,ao,nr,ns,r1,s1,weighting);
