function [Bhat,Ahat,sigm] = hankelw(B,A,nred,Bw,Aw,toler)
% HANKELW Compute a weighted Hankel norm approximation
%         of a rational transfer function
%
%         [BHAT,AHAT] = HANKELW(B,A,NRED,BW,AW)
%
%         A weighted optimal Hankel norm approximation BHAT(s)/AHAT(s)
%         of order NRED is computed for B(s)/A(s). The weighting function
%         BW(s)/AW(s) must have all poles and zeros in the open right half
%         plane and degrees of the polynomials AW and BW must be the same.

% Mats Lilja
% LastEditDate : Thu May 30 09:35:36 1991
% Copyright (c) 1991 by Mats Lilja and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

if nargin < 6,
  toler = 1e-12;
end;
Aw = Aw/Aw(1);
Bw = Bw/Bw(1);
aaw = conv(A,Aw);
bbw = conv(B,Bw);
[Bpol,Apol] = stabparc(bbw,aaw);
na = length(Apol);
nb = length(Bpol);
nn1 = 1:(na-1);
nn2 = na:(2*na-2);
m1 = sylvest(Apol,-Bpol,na-1,na-1);
m2 = -diag(1-rem(1:2*na-2,2)*2)*sylvest(0,Apol,na-1,na-1);
[alfa,beta,q,z,v] = qz(m1,m2);
alfa = diag(alfa);
beta = diag(beta);
d = alfa(nn2)./beta(nn2);
v = v(:,nn2);
[dd,ii] = sort(-abs(d));
d = d(ii);
v = v(:,ii);
v = real(v/diag(v(na,:)));
sigm = abs(d);
signature = d./sigm;
ab = real(v(:,nred+1))';
ab = ab/ab(na);
bb = conv(ab(nn1),Aw);
aa = conv(ab(nn2),Bw);
[bb,aa] = stabparc(bb,aa);
[q,r] = deconv(aa,Bw);
if max(abs(r))<1e-12;
  aa = q;
  bb = dab(Bw,aa,bb);
end;
ah = aa;
[qq,bh] = deconv(bb,aa);
bh(1:min(find(abs(bh)>eps))-1) = [];
Ahat = ah;
Bhat = bh;
G0 = B(length(B))/A(length(A));
if length(B) < length(A),
  Ginf = 0;
else
  Ginf = B(1)/A(1);
end;
Gh0 = bh(length(bh))/ah(length(ah));
if length(bh) < length(ah),
  Ghinf = 0;
else
  Ghinf = bh(1)/ah(1);
end;
Gw0 = Bw(length(Bw))/Aw(length(Aw));
if length(Bw) < length(Aw),
  Gwinf = 0;
else
  Gwinf = Bw(1)/Aw(1);
end;
heur_direct = (Ginf*Gwinf + Gw0*(G0 - Gh0))/(Gwinf + Gw0);
Bhat = addpoly(Bhat,heur_direct*Ahat);
