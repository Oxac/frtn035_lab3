function [b,a,the_error] = lsbac(fr,nb,na,weighting)
% LSBAC Fits a rational function to a given continous time
%       process frequency response.
%
%       [B,A,THE_ERROR] = LSBAC(FR,NB,NA,WEIGHTING)
%
%       Fits a rational transfer function B(s)/A(s) to the
%       continuous time frequency response FR using least squares.
%       Deg A = NA and deg B = NB. Weighting may be included (default is
%       unity weighting). The magnitude of the (weighted) closed
%       loop transfer function error is given by THE_ERROR.

% Mats Lilja
% LastEditDate : Tue Jun 12 19:54:44 1990
% Copyright (c) 1990 by Mats Lilja and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

nfr = size(fr)*[1;0];
if nargin<4,
  weighting = ones(1,nfr);
end;
nweight = length(weighting);
if nfr ~= nweight,
  error('Wrong number of weightings');
end;
i = sqrt(-1);
w = fr(:,1).';
z = i*w;
g = fr(:,2).';
[b,a,the_error] = bafit(z,g,nb,na,weighting);
