function frG=read(fil)
%READ
%  Giw=READ(FIL)
%  Reads frequency response from file <fil> and converts
%  to format where the values are stored in columns as
%
%     [  w  G(iw)  ]
%
%  with w in radians.
%  Note that the file <fil> is given with extension

%  Michael Lundh
%  LastEditDate : Thu Nov 16 14:28:53 1989
%  Mikael Petersson
%  LastEditDate : Fri Jan 22 11:47:00 1999
%  EditNotes: Adapt script to handle output from the Java FRA interface

eval(['load ' fil ' -ascii '])
fil = fil(1:min([length(fil),findstr(fil,'.')-1])); % Remove extension
eval(['frG0=' fil ';'])

frG=[frG0(:,1) frG0(:,2).*exp(sqrt(-1)*frG0(:,3)*pi/180.0)];

[dum,ind]=sort(frG(:,1));
frG=frG(ind,:);

%---- make w monotonic
ind = find(diff(frG(:,1))<eps);
while length(ind)>0
  frG(ind,1)=frG(ind,1)-1e-10;
  ind = find(diff(frG(:,1))<eps);
end
