%% Lab 3 Attempt 2
% initlab3
%%
h = 0.05;
%%
y1 = y(102:end);
u1 = u(102:end);

%%
zi = iddata(y1(1:length(y1)/2), u1(1:length(u1)/2), h);
zi = detrend(zi, 0);
zv = iddata(y1((length(y1)/2 + 1):end), u1((length(u1)/2+1):end), h);
zv = detrend(zv, 0);
%% Modelling
na = 3;
nb = 1;
nc = 1;
k = 2;
armax3112 = armax(zi, [na nb nc k]);
present(armax3112);
ym = idsim(zv.InputData,armax3112);
t = h*[1:1:length(zv.OutputData)];
plot(t,zv.OutputData,t,ym);

pzmap(armax3112)

%%
A = armax3112.a; B = armax3112.b;
A = [A zeros(1,k+nb-1-na)];
B(1:k) = [];
B = [B zeros(1,na-k-nb+1)];
%%
figure
margin(armax3112)
figure
nyquist(armax3112)

%% W integral action 1
% Polynomials
n = 5;
wm = 4;
Ar = [1 -1];
Amc = polybutt(n,wm,45);
Aoc = polybutt(n,2*wm,45);
Am = real(poly(exp(roots(Amc)*h)));
Ao = real(poly(exp(roots(Aoc)*h)));

%%
% DAB-equations
[R,S,T] = rstd(1,B,A,1,Am,Ao,Ar);
pzmap(tf(S,R,h))

%%
figure
nyquist(series(tf(S,R,h),tf(B,A,h)));
%%
figure
margin(series(tf(S,R,h),tf(B,A,h)));

save controllerW1 R S T

%% W integral action 2
% Polynomials
n = 5;
wm = .9*2*pi;
Ar = [1 -1];
Amc = polybutt(n,wm,45);
Aoc = polybutt(n,2*wm,45);
Am = real(poly(exp(roots(Amc)*h)));
Ao = real(poly(exp(roots(Aoc)*h)));

%%
% DAB-equations
[R,S,T] = rstd(1,B,A,1,Am,Ao,Ar);
pzmap(tf(S,R,h))

%%
figure
nyquist(series(tf(S,R,h),tf(B,A,h)));
%%
figure
margin(series(tf(S,R,h),tf(B,A,h)));

save controllerW2 R S T

%% Wo integral action 1
% Polynomials
n = 3;
wm = .85*2*pi;
Ar = 1;
Amc = polybutt(n,wm,45);
Aoc = polybutt(n-1,2*wm,45);
Am = real(poly(exp(roots(Amc)*h)));
Ao = real(poly(exp(roots(Aoc)*h)));

%%
% DAB-equations
[R,S,T] = rstd(1,B,A,1,Am,Ao,Ar);
pzmap(tf(S,R,h))

%%
figure
nyquist(series(tf(S,R,h),tf(B,A,h)));
%%
figure
margin(series(tf(S,R,h),tf(B,A,h)));
%%
save controllerWo1 R S T

%% Wo integral action 2
% Polynomials
n = 3;
wm = .5*2*pi;
Ar = 1;
Amc = polybutt(n,wm,45);
Aoc = polybutt(n-1,2*wm,45);
Am = real(poly(exp(roots(Amc)*h)));
Ao = real(poly(exp(roots(Aoc)*h)));

%%
% DAB-equations
[R,S,T] = rstd(1,B,A,1,Am,Ao,Ar);
%%
pzmap(tf(S,R,h))

%%
figure
nyquist(series(tf(S,R,h),tf(B,A,h)));
%%
figure
margin(series(tf(S,R,h),tf(B,A,h)));
%%
save controllerWo2 R S T