function []=p2mat(fil, id1,p1, id2,p2, id3,p3, id4,p4, id5,p5, id6,p6)
% P2MAT Write a polynomials on .M file for other use.
%
%       P2MAT(FILE, ID1,P1, ID2,P2, ID3,P3, ID4,P4, ID5,P5, ID6,P6)
%
%       Polynomials are written on file FILE.M. One to six polynomials
%       may be written.

% Michael Lundh     LastEditDate : Wed Mar  7 16:27:40 1990
% Copyright (c) 1990 by Michael Lundh and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

if rem(nargin-1,2)~=0, error('Wrong number of inputs'); end;

%---- open file
film=[fil '.m'];
i=0;
while exist(film)==2
  i=i+1;
  film=[fil int2str(i) '.m'];
end
disp(' ')
disp(['Out-file is ' film])
disp(' ')

time_stamp=fix(clock);
time_stamp=[sprintf('%g-%g-%g ',time_stamp(1),time_stamp(2),time_stamp(3)) ..
            sprintf('%g:%g:%g',time_stamp(4),time_stamp(5),time_stamp(6))];

fprintf(film,['%% ' film '\n'])
fprintf(film,['%% Created in Matlab at ' time_stamp '\n'])
fprintf(film,'%% \n')

%---- write polynomials
npoly = (nargin-1)/2;

for ip=1:npoly
  eval(sprintf('id=id%g;p=p%g;',ip,ip))
  str = sprintf('%.4g',p(1));
  for il=2:length(p)
    str = [str ' ' sprintf('%.4g',p(il))];
  end
  fprintf(film,[id '=[' str '] \n'])
end;
