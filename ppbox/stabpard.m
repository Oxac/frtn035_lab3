function [Bs,As,Bu,Au,D] = stabpard(B,A)
% STABPARD Compute the stable part of a discrete time transfer function
%
%       [Bs,As] = stabpard(B,A)
%
%       Extracts the strictly causal, stable partial fraction
%       of the transfer function B(z)/A(z). The stability region
%       is defined as the open unit circle.
%
%       [Bs,As,Bu,Au,D] = stabpard(B,A)
%
%       Separates the transfer function B(z)/A(z) into strictly causal stable,
%       strictly causal unstable and non-causal parts according to
%	   
%         B(z)   Bs(z)   Bu(z)
%         ---- = ----- + ----- + D(z)
%         A(z)   As(z)   Au(z)
%

% Mats Lilja
% LastEditDate : Thu May 30 09:13:57 1991
% Copyright (c) 1991 by Mats Lilja and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

[D,B] = deconv(B,A);
B(1:min(find(B))-1) = [];   % remove leading zeros in B
Aroots = roots(A);
As = real(poly(Aroots(find(abs(Aroots)<1))));
Au = real(poly(Aroots(find(abs(Aroots)>=1))));
[Bs,Bu] = dab(Au,As,B);
       	

