function [b,a] = gadd(b1,a1,b2,a2)
% GADD  Calculates the sum of two transfer functions
%
%       [B,A] = GADD(B1,A1,B2,A2)
%
%       Computes polynomials B(s) and A(s) such that
%	   
%         B(s)   B1(s)   B2(s)
%         ---- = ----- + -----
%         A(s)   A1(s)   A2(s)
%

% Mats Lilja
% LastEditDate : Wed Mar 14 10:22:56 1990
% Copyright (c) 1990 by Mats Lilja and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

a = conv(a1,a2);
b = addpoly(conv(b1,a2),conv(a1,b2));
