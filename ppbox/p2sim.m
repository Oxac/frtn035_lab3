function []=p2sim(fil,syst, id1,p1,i1, id2,p2,i2, id3,p3,i3, id4,p4,i4)
% P2SIM Write a polynomials for use in simnon.
%
%       P2SIM(FILE,SYST, ID1,P1,I1, ID2,P2,I2, ID3,P3,I3, ID4,P4,I4)
%
%       Polynomial coefficients for simnon system SYST are
%       written on parameter-file FILE. One to four polynomials
%       may be written. Polynomial Pi is recognized in simnon
%       as IDi with index starting from Ii.

% Michael Lundh     LastEditDate : Wed Mar  7 16:28:13 1990
% Copyright (c) 1990 by Michael Lundh and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

if rem(nargin-2,3)~=0, error('Wrong number of inputs'); end;

%---- open file
filt=[fil '.t'];
i=0;
while exist(filt)==2
  i=i+1;
  filt=[fil int2str(i) '.t'];
end
disp(' ')
disp(['Out-file is ' filt])
disp(' ')

time_stamp=fix(clock);
time_stamp=[sprintf('%g-%g-%g ',time_stamp(1),time_stamp(2),time_stamp(3)) ..
            sprintf('%g:%g:%g',time_stamp(4),time_stamp(5),time_stamp(6))];

fprintf(filt,['" ' filt '\n'])
fprintf(filt,['" Created in Matlab at ' time_stamp '\n'])
fprintf(filt,'" \n')
fprintf(filt,['[' syst '] \n'])

%---- write polynomials
npoly = (nargin-2)/3;

for ip=1:npoly
  eval(sprintf('i=i%g;id=id%g;p=p%g;',ip,ip,ip))
  for il=1:length(p)
    fprintf(filt,[id '%g :  %g \n'],i,p(il))
    i=i+1;
  end
end;
