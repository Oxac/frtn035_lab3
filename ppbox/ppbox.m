% PPBOX	-- A collection of routines for pole placement design
%          and simulation of continuous time closed loop systems.
%          Written by Michael Lundh and Mats Lilja.
%
% Polynomial Synthesis
%
%   RSTC       - Continuous time
%   RSTD       - Discrete time
%
% Simulation and plotting
%
%   YUSIMC     - Simulation of closed loop of continuous systems
%   YUSIMD     - Simulation of closed loop of discrete systems
%   YUPL       - Plot one to four time responses
%   YUSH       - Add one time response to previous plot
%   YUSIGNAL   - Generate input signals for YUSIMC and YUSIMD
%   YUSTAIRS   - Modification of simulation outputs for stair plots
%
%   Simulation of a continuous system controlled by a discrete controller
%   should be performed in SIMNON.
%
% Polynomials
%
%   ADDPOLY    - Add two polynomials
%   POLYC      - Create continuous time polynomials 
%   POLYBUTT   - Create continuous time Butterworth polynomial
%   POLYBESS   - Create continuous time Bessel polynomial
%   PADE       - Pade approximation of time delay
%   MKSYSP     - Generate systems with structured uncertainty
%
% Continuous to discrete conversion
%
%   SAMPLE     - Sampling of continuous system
%   POLYC2D    - Mapping of continuous poles to discrete poles
%
% Transfer Function Manipulation
%
%   GADD       - Add two rational transfer functions
%   STABPARC   - Separate a rational transfer function into stable and
%                unstable partial fractions (continuous time version)
%   STABPARD   - Separate a rational transfer function into stable and
%                unstable partial fractions (discrete time version)
%
% File Output
%
%   RST2SIM    - Write simnon parameter file with RST-regulator
%   P2SIM      - Write simnon parameter file with polynomials
%   P2MAT      - Write matlab .m file with polynomials
%
% Miscellaneous
%
%    A global variable is used. It is defined by executing PPBOX.
%    The global variable is glob_scale.
%
%    Try PPDEMO for a demonstration.
%
% Bugs
%
%    The use of noise in simulation of continuous systems is not correct
%    since the noise is only present at the instants when outputs are
%    calculated.
%
%    Sometimes different (and erroneous) scaling on screen and in meta file.
%    Normally taken care of by using a larger plot window on screen.
%
%    More bugs, reported but not yet fixed are described by "type ppbox.bugs"

% Michael Lundh     LastEditDate : Thu May 30 09:17:42 1991
% Copyright (c) 1991 by Michael Lundh and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

global glob_scale;
