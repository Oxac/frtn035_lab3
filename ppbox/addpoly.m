function p=addpoly(p1,p2)
% ADDPOLY
%       Adds two polynomials of arbitrary degrees
%       P=ADDPOLY(P1,P2)

% Michael Lundh     LastEditDate : Wed Mar  7 16:25:03 1990
% Copyright (c) 1990 by Michael Lundh and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

dn = length(p1)-length(p2);
p  = [zeros(1,-dn) p1] + [zeros(1,dn) p2];
