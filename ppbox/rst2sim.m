function [] = rst2sim(fil,syst,r,s,t,p,paclibflag)
% RST2SIM
%       Write a RST controller for use in simnon.
%
%       RST2SIM(FILE,SYST,R,S,T)
%       RST2SIM(FILE,SYST,R,S,T,P)
%       RST2SIM(FILE,SYST,R,S,T,P,PACLIBFLAG)
%
%       Controller polynomial coefficients for simnon system SYST are
%       written on parameter-file FILE.
%
%       P is an optional argument defining anti-windup characteristic
%       polynomial. Default anti-windup polynomial is given if P=[].
%
%       If PACLIBFLAG is present the parameter-file suits the RST-
%       controllers CRSTREG and DRSTREG in Simnon. For more information
%       see the documentation of CRSTREG and DRSTREG in PACLIB.
%
%       Indices start from 0. First coefficient of R (and P if present
%       and non-empty) must be 1.

% Michael Lundh     LastEditDate : Wed Mar  7 16:34:01 1990
% Copyright (c) 1990 by Michael Lundh and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

rn=max(size(r));
sn=max(size(s));
tn=max(size(t));

if max([sn,tn])>rn,     error('Not Casual regulator'), end;
if abs(r(1)-1)>1.0E-10, error('R(1) not 1'),         , end;

s=[zeros(1,rn-sn) s];
t=[zeros(1,rn-tn) t];

%---- assign not entered inputs
if nargin>5
  if length(p)==0
    p  = [1 zeros(1,rn-1)]; % default p -- not good for cont regul
    aw = 0;
  else
    aw = 1;
  end
  if rn~=max(size(p)),    error('deg P ~= deg R'),end;
  if abs(p(1)-1)>1.0E-10, error('P(1) not 1'),    end;
end
if nargin==7
  if rn>6, error('PACLIB routine allows max degree=5'), end
  syst='reg'; disp(' ');disp('Forcing: syst=reg');
  r  = [r zeros(1,6-rn)];
  s  = [s zeros(1,6-rn)];
  t  = [t zeros(1,6-rn)];
  p  = [p zeros(1,6-rn)];
  rn = 6;
end

%---- open file
filt=[fil '.t'];
i=0;
while exist(filt)==2
  i=i+1;
  filt=[fil int2str(i) '.t'];
end
disp(' ')
disp(['Out-file is ' filt])
disp(' ')

time_stamp=fix(clock);
time_stamp=[sprintf('%g-%g-%g ',time_stamp(1),time_stamp(2),time_stamp(3)) ..
            sprintf('%g:%g:%g',time_stamp(4),time_stamp(5),time_stamp(6))];

fprintf(filt,['" ' filt '\n'])
fprintf(filt,['" Created in Matlab at ' time_stamp '\n'])
fprintf(filt,'" \n')
fprintf(filt,['[' syst '] \n'])

%---- write polynomials

for i=2:rn,
  fprintf(filt,'r%g :  %g \n',i-1,r(i))
end;

for i=1:rn,
  fprintf(filt,'s%g :  %g \n',i-1,s(i))
end;

for i=1:rn,
  fprintf(filt,'t%g :  %g \n',i-1,t(i))
end;

if nargin>5
  for i=2:length(p),
    fprintf(filt,'p%g :  %g \n',i-1,p(i))
  end;
  if nargin==7
    fprintf(filt,'aw :  %g \n',aw)
  end
end;
