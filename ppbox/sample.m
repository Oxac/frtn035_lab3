function [bd,ad]=sample(bc,ac,cdelay,h);
% SAMPLE
%       Sampling of continuous time system.
%
%       [Bd,Ad]=SAMPLE(Bc,Ac,Tsamp);
%       [Bd,Ad]=SAMPLE(Bc,Ac,Tau,Tsamp);
%
%       Computes the discrete time transfer function H(q)=Bd(q)/Ad(q)
%       when sampling the continuous system G(s)= Bc(s)/Ac(s) or
%       G(s)= Bc(s)/Ac(s)*exp(-Tau*s).

% Mats Lilja (original)
% Michael Lundh     LastEditDate : Wed Mar  7 16:36:06 1990
% Copyright (c) 1990 by Michael Lundh and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

  while abs(bc(1))<10000*eps, bc(1)=[]; end

  [a,b,c,d]=tf2ss(bc,ac);

  if nargin==3
    h = cdelay;
    [ax,bx] = c2d(a,b,h);
    [bd,ad] = ss2tf(ax,bx,c,d,1);
    while abs(bd(1)) < 1e-12,
      bd(1) = [];
    end;
    return
  end

  if abs(d)>eps
    error('System must be strictly proper if a delay is present')
  end

  ddelay = round(cdelay/h + 0.5)-1;
  tau = cdelay - h*ddelay;
  [fi,gam]=c2d(a,b,h);
  [slask,gam0]=c2d(a,b,h-tau);
  gam1 = gam - gam0;
  bigfi = [fi gam1; 0*c 0*d];
  biggam = [gam0 ; 1 ];
  bigc = [c 0*d];
  [bd,ad]=ss2tf(bigfi,biggam,bigc,d,1);
  ee=eye(1,ddelay+1);
  ad = conv(ad,ee);
  while max(abs([bd(length(bd)) ad(length(ad))])) < 1e-12,
    ad(length(ad)) = [];
    bd(length(bd)) = [];
  end;
  while abs(bd(1)) < 1e-12,
    bd(1) = [];
  end;
