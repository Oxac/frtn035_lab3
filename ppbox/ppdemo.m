% PPBOXDEMO
%       A demonstration of how PPBOX and FRBOX may be used for pole
%       placement design.

% Michael Lundh     LastEditDate : Wed Mar 21 22:42:36 1990
% Copyright (c) 1990 by Michael Lundh and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

echo off
ppbox
frbox
echo on

%---- The open system: 1/(s+1)^3
b = 1;
a = [1 3 3 1];

%---- Pole placement design
am = polybutt(3,1.2,45)
ao = polybutt(3,2.0,45)

[r,s,t]=rstc(1,b,a,1,am,ao,[1 0])

%---- Hit any key to continue
pause

%---- Simulation
yu0 = yusimc(b,a,r,s,t,30);
yupl(yu0)
title('Nominal closed loop system'), pause

%---- Hit any key to continue
pause

%---- structured uncertainty in gain and one pole
[bb,aa] = mksysp(1,[1 1],0.4,[0 0.4],1,[1 2 1])
yuz     = yusimc(bb,aa,r,s,t,30);
yupl(yuz), pause

%---- Hit any key to continue
pause

yush(yu0,'+w')
title('Nominal and disturbed systems'), pause

%---- Hit any key to continue
pause

%---- frequency resonses
gp  = frc(b,a,0,-2,2);
gpz = frc(bb,aa,0,-2,2);

gff = frc(t,r,0,-2,2);
gfb = frc(s,r,0,-2,2);

lz  = fmul(gfb,gpz);

bopl(gpz)
title('Disturbed open systems'), pause

%---- Hit any key to continue
pause

bopl(lz)
title('Disturbed compensated open systems'), pause
%---- Hit any key to continue
pause

nypl(lz)
nygrid
title('Disturbed compensated open systems'), pause
%---- Hit any key to continue
pause

evpl(gp,gfb,gff), pause
%---- Hit any key to continue
pause

evpl(gpz,gfb,gff), pause

echo off
