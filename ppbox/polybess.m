function p = polybess(n,r)
% POLYBESS
%       Computes a Bessel polynomial of specified order.
%
%       P = POLYBESS(N,R)
%
%       A Bessel polynomial of order N is computed. The zeros of
%       the polynomial are scaled with a factor of R, which means
%       that each zero has a magnitude of approximately R.

% Mats Lilja     LastEditDate : Wed Mar  7 16:29:56 1990
% Copyright (c) 1990 by Mats Lilja and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

if n == 0,
  p = 1;
elseif n == 1,
  p = [1 1];
elseif n > 1,
  p0 = 1;
  p1 = [1 1];
  for j=2:n,
    p = [0 (2*j-1)*p1] + [p0 0 0];
    p0 = p1;
    p1 = p;
  end;
else,
  error('Argument must be a non-negative integer');
end
if nargin == 2,
  np = length(p) - 1;
  p = p.*(r^np/p(np+1)).^((0:np)/np);
end;
