function tryu2=yustairs(tryu1,ryu)
%YUSTAIRS
%        Modification of simulation outputs.
%
%        TRYU2=YUSTAIRS(TRYU1)
%        TRYU2=YUSTAIRS(TRYU1,RYU)
%
%        The function modifies the output from the routines YUSIMD to make
%        the plot more discrete like.  Additional points are added in order
%        to plot some of the signals r, y and u as stairs, as if they were
%        outputs from zero order hold circuits.
%        The optional parmeter RYU is any combination of 'u' 'y' 'r' to
%        define which signal that will be modified. Default is 'ru'.
%
%        Warning: The output from this function is recomended for plotting
%        only.

% Michael Lundh     LastEditDate : Tue Mar 20 13:11:39 1990
% Copyright (c) 1990 by Michael Lundh and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

if nargin==1; ryu='ru'; end

fixr = any('r'==ryu) | any('R'==ryu);
fixy = any('y'==ryu) | any('Y'==ryu);
fixu = any('u'==ryu) | any('U'==ryu);

[m,n] = size(tryu1);

%---- time column
ci    = [tryu1(1:m-1,1) tryu1(2:m,1)]';
tryu2 = ci(:);

%---- reference
if fixr
  ci = [tryu1(1:m-1,2) tryu1(1:m-1,2)]';
else
  ci = [tryu1(1:m-1,2) tryu1(2:m,2)]';
end
tryu2 = [tryu2 ci(:)];


for i=3:n
  if (rem(i,2)==1 & fixy) | (rem(i,2)==0 & fixu)
    ci = [tryu1(1:m-1,i) tryu1(1:m-1,i)]';
  else
    ci = [tryu1(1:m-1,i) tryu1(2:m,i)]';
  end
  tryu2 = [tryu2 ci(:)];
end
