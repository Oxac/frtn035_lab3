function trldn=yusignal(t,rlev,rch,llev,lch,dlev,dch,nvar,nch)
% YUSIGNAL
%       Create signals for simulation.
%
%       TRLDN=YUSIGNAL(TIME,RLEV,RCH,LLEV,LCH,DLEV,DCH,NVAR,NCH)
%
%       Signal generator for TRLDN in YUSIMC and YUSIMD. A matrix
%
%         TRLDN=[t r l d n]
%
%       is created.  Its first column defines the simulation time, other
%       columns correspond to reference signal r, input disturbance l,
%       output disturbance d and measurement noise n respectively at the
%       time instants of first column.
%
%       Each signal s is defined by two vectors sLEV and sCH. Initially
%       s is zero. Changes occur at occasions in vector sCH. The signal
%       s is sLEV from time sCH.
%       NVAR is variance for Gaussian noise from time NCH.
%
%       The function may have 1, 3, 5, 7 or 9 input arguments.
%       In case of only one argument, ref=1 from time=0, l=-1
%       from max(TIME)/3, d=-1 from max(TIME)*2/3 and n=0;
%       In case of more input arguments signals not being referenced
%       to are zero.

% Michael Lundh     LastEditDate : Thu May 30 09:05:36 1991
% Copyright (c) 1991 by Michael Lundh and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

if rem(nargin,2)~=1, error('wrong number of inputs'); end

epps = 10000*eps;
tmax = max(t);

t=t(:); 
l=0*t; d=0*t; n=0*t;

narginZ = nargin;
if narginZ==1
  narginZ=7;
  rlev=1 ; rch=0;
  llev=-1; lch=tmax/3;
  dlev=-1; dch=tmax*2/3;
end

rch=[rch tmax+1];
for i = 1:length(rlev) 
  ind    = find( ((rch(i)-epps)<=t) & (t<rch(i+1)) );
  r(ind) = rlev(i)*ones(1,length(ind));
end

if narginZ>3
  lch=[lch tmax+1];
  for i = 1:length(llev)
    ind    = find( ((lch(i)-epps)<=t) & (t<lch(i+1)) );
    l(ind) = llev(i)*ones(1,length(ind));
  end;
end

if narginZ>5
  dch=[dch tmax+1];
  for i = 1:length(dlev)
    ind    = find( ((dch(i)-epps)<=t) & (t<dch(i+1)) );
    d(ind) = dlev(i)*ones(1,length(ind));
  end;
end

if narginZ>7
  rand('normal');rand('seed',0)
  nn=rand(length(t),1);
  nch=[nch tmax+1];
  for i = 1:length(nvar)
    ind    = find( ((nch(i)-epps)<=t) & (t<nch(i+1)) );
    n(ind) = sqrt(nvar(i))*nn(ind);
  end;
end

trldn=[t(:) r(:) l(:) d(:) n(:)];
