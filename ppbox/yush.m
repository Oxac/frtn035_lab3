function yush(tr,option)
% YUSH  Show an YU plot in a previously drawn YU diagram.
%
%       YUSH(TR,OPTION)
%
%       An YU plot is done from the time responses in TR using
%       plot-option OPTION. The argument option is optional.
%
%       The scales of the y and u plot are taken from the
%       variable glob_scale.  This variable is assumed
%       having been declared as global. 

% Michael Lundh     LastEditDate : Tue Jun 18 08:34:01 1991
% Copyright (c) 1990 by Michael Lundh and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

if length(glob_scale) < 1,
  error('No previous YU plot');
end;

[m,n]=size(tr);
t1=tr(:,1);  r1=tr(:,2);  y1=tr(:,3:2+(n-2)/2);  u1=tr(:,3+(n-2)/2:n);

subplot(211);
axis(glob_scale(1,:));
if nargin == 1,
  plot(t1,y1,t1,r1,'-w');
else
  plot(t1,y1,option,t1,r1,'-w');
end;

subplot(212);
axis(glob_scale(2,:));
if nargin == 1,
  plot(t1,u1)
else
  plot(t1,u1,option)
end;
axis;
subplot(211);
