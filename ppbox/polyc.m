function ac=polyc(w,z,p);
% POLYC Make Continuous time polynomial.
%
%       P=POLYC(W,Z,R)
%
%       Polynomial formed as product of first and second order
%       systems with real zeros R(i) and complex zeros with
%       natural frequencies W(i) and damping Z(i).
%       Argument R is optional.

% Michael Lundh     LastEditDate : Wed Mar  7 16:31:15 1990
% Copyright (c) 1990 by Michael Lundh and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

if length(w)~=length(z), error('length(w)~=length(z)'); end;

ac=1;
for i=1:length(w)
  ac=conv(ac,[1 2*w(i)*z(i) w(i)*w(i)]);
end

if nargin==3,
  ac=conv(ac,poly(p));
end
