function notdef = ppcheck
% PPCHECK checks if the global variables needed for PPBOX have been defined

% Michael Lundh     LastEditDate : Tue Mar 13 22:39:33 1990
% Copyright (c) 1990 by Michael Lundh and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

if exist('glob_scale')~=1
  disp('Defining necessary global variables. Please reissue command.');
  ppbox;
  notdef = 1;
else
  notdef = 0;
end


