function [bb,aa]=mksysp(b,a,db,da,b0,a0)
% MKSYSP  
%       Generate polynomials for structured uncertainty.
%
%       [BB,AA]=MKSYSP(B,A,DB,DA)
%       [BB,AA]=MKSYSP(B,A,DB,DA,B0,A0)
%
%       Generate all extreme parameter combinations of the system
%
%              BP   B0
%         G = ---- ----
%              AP   A0
%
%       where B0 and A0 are known and the coefficients of BP/AP
%       satisfy
%
%         B(i)-DB(i) <= BP(i) <= B(i)+DB(i)
%         A(i)-DA(i) <= AP(i) <= A(i)+AB(i)
%
%       The output matrices BB and AA have 2^I rows, where I is the
%       numberof nonzero elements in [DB DA].  The polynomials BB(j,:)
%       and AA(j,:) defines the system corresponding to the j:th corner
%       in the polytope of polynomial coefficients.

% Michael Lundh     LastEditDate : Wed Mar  7 16:26:53 1990
% Copyright (c) 1990 by Michael Lundh and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

if max(size(db))==0, db=0*b; end;
if max(size(da))==0, da=0*a; end;

ba  = [b a];
dba = [db da];

nb  = length(b);
na  = length(a);
nba = na+nb;

Index = [];
for i=1:nba,
  if abs(dba(i))>1.0e-10, Index = [Index i]; end;
end;

nIndex = length(Index);
msys   = 2^nIndex;
bbaa   = kron(ba,ones(msys,1));

%---- gray table
g=[0;1];
[m,n]=size(g);
while m<msys
  g=[zeros(m,1) g ; ones(m,1) rot90(eye(m,m))*g];
  [m,n]=size(g);
end

for m=1:msys
  for i=1:nIndex,
    bbaa(m,Index(i)) = bbaa(m,Index(i)) + (2*g(m,i)-1)*dba(Index(i));
  end
end

bb = bbaa(:,1:nb);
aa = bbaa(:,nb+1:nba);

if nargin==6
  for m=1:msys
    bbx(m,:) = conv(bb(m,:),b0);
    aax(m,:) = conv(aa(m,:),a0);
  end
  bb = bbx;
  aa = aax;
end
