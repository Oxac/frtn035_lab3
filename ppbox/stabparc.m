function [Bs,As,Bu,Au,D] = stabparc(B,A)
% STABPARC Compute the stable part of a continuous time transfer function
%
%       [Bs,As] = stabparc(B,A)
%
%       Extracts the strictly proper, stable partial fraction
%       of the transfer function B(s)/A(s). The stability region
%       is defined as the open left half plane.
%
%       [Bs,As,Bu,Au,D] = stabparc(B,A)
%
%       Separates the transfer function B(s)/A(s) into strictly proper stable,
%       strictly proper unstable and non-proper parts according to
%	   
%         B(s)   Bs(s)   Bu(s)
%         ---- = ----- + ----- + D(s)
%         A(s)   As(s)   Au(s)
%

% Mats Lilja
% LastEditDate : Thu May 30 09:13:26 1991
% Copyright (c) 1991 by Mats Lilja and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

[D,B] = deconv(B,A);
B(1:min(find(B))-1) = [];   % remove leading zeros in B
Aroots = roots(A);
As = real(poly(Aroots(find(real(Aroots)<0))));
Au = real(poly(Aroots(find(real(Aroots)>=0))));
[Bs,Bu] = dab(Au,As,B);
       	

