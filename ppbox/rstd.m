function [r,s,t]=rstd(bplus,bminus,a,bm1,am,ao,ar,as);
% RSTD  Polynomial synthesis in discrete time.
%
%       [R,S,T]=RSTD(BPLUS,BMINUS,A,BM1,AM,AO,AR,AS);
%       [R,S,T]=RSTD(BPLUS,BMINUS,A,BM1,AM,AO,AR);
%       [R,S,T]=RSTD(BPLUS,BMINUS,A,BM1,AM,AO);
%
%       Polynomial synthesis according to CCS ch 10 to
%       design a controller R(q) u(k) = T(q) r(k) - S(q) y(k)
%
%       Inputs:  BPLUS  : Part of open loop numerator
%                BMINUS : Part of open loop numerator
%                A      : Open loop denominator
%                BM1    : Additional zeros
%                AM     : Closed loop denominator
%                AO     : Observer polynomial
%                AR     : Pre-specified factor of R,
%                         e.g integral part [1 -1]**k
%                AS     : Pre-specified factor of S,
%                         e.g notch filter [1 0 w^2]
%
%       Outputs: R,S,T  : Polynomials in controller
%
%       See function DAB how the solution to the Diophantine-
%       Aryabhatta-Bezout identity is chosen.

% Michael Lundh     LastEditDate : Wed Mar 21 14:35:36 1990
% Copyright (c) 1990 by Michael Lundh and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

if nargin==7, as=1; elseif nargin==6, ar=1; as=1; end;

ae      = conv(a,ar);
be      = conv(bminus,as);
aoam    = conv(am,ao);
[r1,s1] = dab(ae,be,aoam);

r       = conv(conv(r1,ar),bplus);
s       = conv(s1,as);

bm      = conv(bminus,bm1);
t0      = sum(am)/sum(bm);
t       = t0*conv(ao,bm1);

s       = s/r(1);
t       = t/r(1);
r       = r/r(1);
