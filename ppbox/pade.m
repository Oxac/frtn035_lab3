function [b,a] = pade(l,ord)
% PADE  Computes Pade' approximation B(s)/A(s) of order ORD to exp(-sL)
%       
%       [B,A] = PADE(L,ORD)
%
%	ORD is 1 or 2.

% Michael Lundh     LastEditDate : Wed Mar  7 16:28:52 1990
% Copyright (c) 1990 by Michael Lundh and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

if abs(l)<eps, b=1;
               a=1;
               return;
end;

if ord==1,     b=[-l 2];
               a=[ l 2];
elseif ord==2, b=[l^2 -6*l 12];
               a=[l^2  6*l 12];
else
  error('ORD must be 1 or 2')
end
