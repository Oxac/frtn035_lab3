function butterpoly = polybutt(order,radius,angle)
% POLYBUTT
%       Make continuous time polynomial.
%
%       P = POLYBUTT(ORDER,RADIUS,ANGLE)
%
%       Calculates a polynomial of order 'ORDER' with roots evenly spread
%       on a circle segment with radius 'RADIUS' in left half plane. 
%       The third argument is half the opening angle of the segment.
%       The default value for 'ANGLE' is 90(1-1/'ORDER') degrees
%       (ordinary Butterworth).

% Mats Lilja     LastEditDate : Wed Mar  7 16:30:33 1990
% Copyright (c) 1990 by Mats Lilja and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

if nargin == 2 & order > 0,
  angle = 90*(1-1/order);
end;
i = sqrt(-1);
if order > 1,
  dangle = 2*angle/(order-1);
  d = dangle/10;
  butterpoly = real(poly(radius*exp(i*(180-angle:dangle:180+angle+d)*pi/180)));
elseif order == 1,
  butterpoly = [1 radius];
else
  butterpoly = 1;
end;
