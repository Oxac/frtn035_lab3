function ad=polyc2d(ac,h);
%POLYC2D
%       Make discrete time polynomial.
%
%       AD=POLYC2D(AC,H)
%
%       Discrete characteristic polynomial AD that is given when a
%       continuous system with characteristic polynomial AC is sampled
%       with sampling interval H.

% Michael Lundh     LastEditDate : Tue May  7 08:53:43 1991
% Copyright (c) 1991 by Michael Lundh and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

rch = roots(ac)*h;
ad  = real(poly(exp(rch)));

if abs(imag(rch))>pi
  disp([setstr(7) 'Warning:  max( im(roots(ac)*h )>pi']);
end;


