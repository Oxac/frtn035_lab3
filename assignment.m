%% Identification and Controller Design

%% The identification Experiment
h = 0.05;
T = 1.07;
omega = 1*2*pi/T;
h*omega


%% The identification
ident
%%
plot([y u])
%%
z = [y(101:end) u(101:end)];
z = detrend(z, 'constant');
mscohere(u,y,[],[],[],1/h);
%%
w = logspace(0,2,200);
g = spa(z,50)

plot(g);
%%
y = y(102:end);
u = u(102:end);
nz = size(y,1);
z1 = iddata(y(1:nz/2),u(1:nz/2),h);
z2 = iddata(y(nz/2+1:nz),u(nz/2+1:nz),h);
z1 = detrend(z1,0);
z2 = detrend(z2,0);
%%
% Persistence of excitation:
N = 50;
thlarge = armax(z1, [N N N 1]);
%%
[A, B, C, D] = ssdata(thlarge);
[Ab, Bb, Cb, M, T] = dbalreal(A, B, C);
n = 5;
[Ab, Bb, Cb, Db] = dmodred(Ab, Bb, Cb, D, n:50);
[b, a] = ss2tf(Ab, Bb, Cb, Db);
%%
figure
th3112 = armax(z1, [3 1 1 2]);
present(th3112);
ym = idsim(z2.InputData,th3112);
t = h*[1:1:length(z2.OutputData)];
plot(t,z2.OutputData,t,ym);
%%
pzmap(th3112)
%%
th4112 = armax(z1, [4 1 1 2]);
present(th4112);
ym = idsim(z2.InputData,th4112);
t = h*[1:1:length(z2.OutputData)];
plot(t,z2.OutputData,t,ym);
%%
pzmap(th4112)
%%
figure
th3222 = armax(z1, [3 2 2 2]);
present(th3222);
ym = idsim(z2.InputData,th3222);
t = h*[1:1:length(z2.OutputData)];
plot(t,z2.OutputData,t,ym);
%%
th4222 = armax(z1, [4 2 2 2]);
present(th4222);
ym = idsim(z2.InputData,th4222);
t = h*[1:1:length(z2.OutputData)];
plot(t,z2.OutputData,t,ym);
%%
pzmap(th4222)
%%
k = 2;
na = 3;
nb = 1;
nc = 1;
A = th3112.a; B = th3112.b;
A = [A zeros(1,k+nb-1-na)];
B(1:k) = [];
B = [B zeros(1,na-k-nb+1)];

%% The controller design
figure
margin(th3222)
figure
nyquist(th3222)

%% W integral action 1
% Polynomials
n = 10;
wm = 5;
Ar = [1 -1];
Amc = polybutt(n,wm,45);
Aoc = polybutt(n,2*wm,45);
Am = real(poly(exp(roots(Amc)*h)));
Ao = real(poly(exp(roots(Aoc)*h)));

%%
% DAB-equations
[R,S,T] = rstd(1,B,A,1,Am,Ao,Ar);
pzmap(tf(S,R,h))

%%

nyquist(series(tf(S,R,h),tf(B,A,h)));
%%
margin(series(tf(S,R,h),tf(B,A,h)));

save controllerW1 R S T
%% W integral action 2
% Polynomials
Ar = [1 -1];
Amc = polybutt(n,wm,45);
Aoc = polybutt(n-1,2*wm,45);
Am = real(poly(exp(roots(Amc)*h)));
Ao = real(poly(exp(roots(Aoc)*h)));

%%
% DAB-equations
[R,S,T] = rstd(1,B,A,1,Am,Ao,Ar);
nyquist(series(tf(S,R,h),tf(B,A,h)));

save controllerW2 R S T
%% Wo integral action 1
% Polynomials
wm = 5 %0.5*2*pi;
Ar = 1;
n = 3;
Amc = polybutt(n,wm,45);
Aoc = polybutt(n,2*wm,45);
Am = real(poly(exp(roots(Amc)*h)));
Ao = real(poly(exp(roots(Aoc)*h)));

%%
% DAB-equations
[R,S,T] = rstd(1,B,A,1,Am,Ao,Ar);

pzmap(tf(S, R, h))
%%

nyquist(series(tf(S,R,h),tf(B,A,h)));
%%
margin(series(tf(S,R,h),tf(B,A,h)))
save controllerWo1 R S T
%% Wo integral action 2
% Polynomials
Ar = 1;
Amc = polybutt(n,wm,45);
Aoc = polybutt(n-1,2*wm,45);
Am = real(poly(exp(roots(Amc)*h)));
Ao = real(poly(exp(roots(Aoc)*h)));

%%
% DAB-equations
[R,S,T] = rstd(1,B,A,1,Am,Ao,Ar);
nyquist(series(tf(S,R,h),tf(B,A,h)));

