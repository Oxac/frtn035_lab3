function f=add(f1,f2);
%ADD
%  Giw=ADD(Giw1,Giw2)
%  Merges frequency responses Giw1 and Giw2. Epsilon is added
%  to the frequencies w such that MIN(DIFF(Giw(:,1)) > 0.

%  Michael Lundh
%  LastEditDate : 1989-08-11

f = [f1 ; f2];
[dum,ind]=sort(f(:,1));
f=f(ind,:);

%---- make w monotonic
ind = find(diff(f(:,1))<eps);
while length(ind)>0
  f(ind,1)=f(ind,1)-1e-10;
  ind = find(diff(f(:,1))<eps);
end

