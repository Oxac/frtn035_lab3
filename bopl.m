function bopl(fr1,fr2,fr3,fr4,scale)
% BOPL  Plot a bode plot.
%
%       bopl(fr1,fr2,fr3,fr4,scale)
%
%       A bode plot is done from the frequency responses in fr1 - fr4. 
%       The arguments fr2 - fr4 are optional. fr1 - fr4 is allowed to have
%       different number of data points and columns.
%
%	The optional argument scale is used to affect the scaling of the 
%       plot. It takes the form [wmin wmax amin amax phmin phmax]. wmin, 
%       scale can be used even if fr2 - fr4 are omitted.
%
%       The frequency axis will be marked in rad/s (default) or Hz depending
%       on if setrad or sethz has been called.

% Kjell Gustafsson    
% LastEditDate : Wed Sep 22 16:13:06 1993
% Copyright (c) 1990 by Kjell Gustafsson and Department of Automatic Control,
% Lund Institute of Technology, Lund, SWEDEN

% Mikael Petersson
% LastEditDate : Thu Jan 21 17:26:00 1999
%

global glob_hz;
global bode_handle_mag bode_handle_ph;

colors = 'rgbm';

error(nargchk(1,5,nargin))

if nargin~=5
  scale = eval(['fr' int2str(nargin)]);
end
scaleinfo = all(size(scale) == [1 6]);

clf reset

bode_handle_mag = subplot(2,1,1);
for k=1:nargin-scaleinfo
  kstr = int2str(k);
  eval(['ind = sprintf(''(:,2:%g)'',size(fr' kstr ',2));']);
  if ~isempty(glob_hz)
    eval(['loglog(fr' kstr '(:,1)/(2*pi),abs(fr' kstr ind '), colors(k) )']);
  else
    eval(['loglog(fr' kstr '(:,1),abs(fr' kstr ind '),colors(k) )']);
  end
  hold on
end

if scaleinfo
  axis(scale(1:4)); 
end

hold off

ylabel('Magnitude');

bode_handle_ph = subplot(2,1,2);
for k=1:nargin-scaleinfo
  kstr = int2str(k);
  eval(['ind = sprintf(''(:,2:%g)'',size(fr' kstr ',2));']);
  if ~isempty(glob_hz)
    eval(['semilogx(fr' kstr '(:,1)/(2*pi),arg(fr' kstr ind '), colors(k) )']);
  else
    eval(['semilogx(fr' kstr '(:,1),arg(fr' kstr ind '),colors(k) )']);
  end
  hold on
end

if scaleinfo
  axis(scale([1,2,5,6])); 
end

hold off

ylabel('Phase [deg]');

if ~isempty(glob_hz),
  xlabel('Frequency [Hz]');
else
  xlabel('Frequency [rad/s]');
end

subplot(bode_handle_mag);   % To make `title' appear at the top





