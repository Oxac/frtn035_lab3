function coh = coher(u,y,h);
%COHER     Function that computes and plots the coherence function.
%
%          function coh = coher(u,y,h);
%
%          Computes and plots the coherence function 
%          between u and y. h is the sampling period.

phiu = spa(u,[],[],[],h)*[0;1;0];
phiy = spa(y,[],[],[],h)*[0;1;0];
Gsp = spa([y u],[],[],[],h);
absphiyu = Gsp(2:end,2).*phiu(2:end);
coh = absphiyu./sqrt(phiu(2:end))./sqrt(phiy(2:end));
semilogx(Gsp(2:end,1),coh);
xlabel('rad/s');

