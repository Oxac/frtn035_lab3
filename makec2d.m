% Convert controller and feedforward-filter to discrete form
%
if exist('h')~=1,
   error('Sample time h not specified. Script not run. Specify h and retry.')
end

% Continuous controller
Gc = tf(S,R)
%
% Discrete controller
Gcd = c2d(Gc,h,'tustin')
%
% Extract polynomials
[Sd,Rd,hd,dd]=tfdata(Gcd,'v')
%
% RST representation
Td = Sd

% Continuous FF-filter
Gff = tf(Bff,Aff)
%
% Discrete FF-filter
Gffd = c2d(Gff,h,'tustin')
%
% Extract polynomials
[Bffd,Affd,hd,dd]=tfdata(Gffd,'v')

% NB! Use fixed step solver in simulation
